import KapRepo from "../repo/kap";
import IRequestHandler from "../models/requestHandler";

export default class KapController {
	private kapRepo: KapRepo;

	constructor() {
		this.kapRepo = new KapRepo();
	}

	getAllKaps: IRequestHandler = async (req, res, next) => {
		try {
			const { page = 1, pageSize = 30 } = req.params;

			const kaps = await this.kapRepo.getAll(page, pageSize);
			const count = await this.kapRepo.count();

			const data = {
				items: kaps,
				total: count,
				page,
				pageSize
			};

			res.ok(data);
		} catch (err) {
			next(err);
		}
	};

	createKap: IRequestHandler = async (req, res, next) => {
		try {
			const kap = await this.kapRepo.add(req.body);
			res.ok(kap);
		} catch (err) {
			next(err);
		}
	};
}
