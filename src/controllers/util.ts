import KapRepo from "../repo/kap";
import IRequestHandler from "../models/requestHandler";

export default class UtilController {
	private kapRepo: KapRepo;

	constructor() {
		this.kapRepo = new KapRepo();
	}

	getFeed: IRequestHandler = async (req, res, next) => {
		try {
			const todayKap = await this.kapRepo.getByDay(new Date());

			const data = {
				todayKap
			};
			res.ok(data);
		} catch (err) {
			next(err);
		}
	};
}
