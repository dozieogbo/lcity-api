import pick from "lodash/pick";
import UserRepo from "../repo/user";
import JWT from "../util/jwtservice";
import Auth from "../util/authservice";
import IRequestHandler from "../models/requestHandler";

export default class UserController {
	private userRepo: UserRepo;

	constructor() {
		this.userRepo = new UserRepo();
	}

	login: IRequestHandler = async (req, res, next) => {
		try {
			const user = await this.userRepo.getByEmail(req.body.username);
			const isPwdCorrect = user && (await user.hasPassword(req.body.password));

			if (isPwdCorrect) {
				const token = await JWT.encode(user.id);
				const data = {
					...token,
					...user.toObject()
				};

				res.ok(data);
			} else res.unauthorized("Invalid login details");
		} catch (err) {
			next(err);
		}
	};

	signup: IRequestHandler = async (req, res, next) => {
		try {
			const body = pick(req.body, "firstName", "lastName", "email", "password");

			const user = await this.userRepo.add(body);

			const token = await JWT.encode(user.id);

			const data = {
				...token,
				...user.toObject()
			};
			res.json(data);
		} catch (err) {
			next(err);
		}
	};

	changePwd: IRequestHandler = async (req, res, next) => {
		try {
			const { password, newPassword } = req.body;
			const { user } = req;

			const hasPassword = user.hasPassword(password);

			if (hasPassword) {
				const update = {
					password: await Auth.hashPassword(newPassword)
				};

				await this.userRepo.update(user.id, update);

				res.ok({});
			} else {
				res.unauthorized("Incorrect password provided");
			}
		} catch (err) {
			next(err);
		}
	};
}
