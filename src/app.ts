import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import dotenv from "dotenv";
import compression from "compression";

dotenv.config();

import routes from "./routes";
import db from "./config/db";
import { initialize } from "./util/middleware";
import errorHandler from "./util/errorHandler";

const app = express();

app.use(compression());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(initialize);
app.use(routes);
app.use(errorHandler);

db.init();

module.exports = app;
