import { IUser } from "./db/user";
import { NextFunction, Response, Request } from "express";

export default interface IRequestHandler {
	(req: IAuthRequest, res: IResponse, next: NextFunction): any;
}

export interface IAuthRequest extends Request {
	user: IUser;
	isAdmin: boolean;
}

export interface IResponse extends Response {
	ok: (data: any, message?: string) => void;
	invalid: (errors: string[], message?: string) => void;
	unauthorized: (message?: string) => void;
	error: (error: string | Error) => void;
}
