import { Schema, SchemaDefinition, SchemaOptions } from "mongoose";

export class DBSchema extends Schema {
	constructor(definition: SchemaDefinition) {
		const options: SchemaOptions = {};

		options.timestamps = true;
		options.toObject = options.toJSON = {
			virtuals: true,
			versionKey: false,
			transform: (doc, ret) => {
				delete ret._id;
				delete ret.password;
			}
		};

		super(definition, options);
	}
}
