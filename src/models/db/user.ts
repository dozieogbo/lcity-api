import { Document, model, SchemaType } from "mongoose";
import { createHash } from "crypto";
import { DBSchema } from "./schema";
import Auth from "../../util/authservice";
import { MongoError } from "mongodb";
import { NonUniqueEmailError, NonUniquePhoneError } from "../../util/errors";
import { USER, ADMIN } from "../../util/constants";

export interface IUser extends Document {
	firstName: string;
	lastName: string;
	password: string;
	email: string;
	phone?: string;
	displayUrl: string;
	isAuthor: boolean;
	active: boolean;
	verified: boolean;
	role: string;
	createdAt: Date;
	updatedAt: Date;
	hasPassword: (password: string) => boolean;
}

export const UserSchema = new DBSchema({
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	email: { type: String, required: true, unique: true },
	phone: { type: String, required: false, unique: true, sparse: true },
	displayUrl: { type: String, required: false },
	password: { type: String, required: true },
	role: { type: String, default: USER, enum: [USER, ADMIN] },
	isAuthor: { type: Boolean, default: false },
	active: { type: Boolean, default: true },
	verified: { type: Boolean, default: false }
});

UserSchema.methods.hasPassword = async function(password: string) {
	return await Auth.verifyPassword(password, this.password);
};

UserSchema.pre<IUser>("save", async function(next) {
	if (this.isModified("password")) {
		this.password = await Auth.hashPassword(this.password);
	}
	if (this.isNew) {
		const hash = createHash("md5")
			.update(this.email)
			.digest("hex");
		this.displayUrl = `https://gravatar.com/avatar/${hash}?s=200&d=retro`;
	}
	next();
});

UserSchema.post("save", function(
	err: MongoError,
	doc: any,
	next: (err?: Error) => void
) {
	if (err && err.code === 11000) {
		if (err.errmsg.includes("email")) next(new NonUniqueEmailError());
		else if (err.errmsg.includes("phone")) next(new NonUniquePhoneError());
	} else next(err);
});

const user = model<IUser>("User", UserSchema);

export default user;
