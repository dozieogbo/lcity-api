import { Document, model, SchemaType } from "mongoose";
import { DBSchema } from "./schema";
import KapRepo from "../../repo/kap";

export interface IKap extends Document {
	text: string;
	focalPoint: string;
	prayers: Array<string>;
	createdAt: Date;
	updatedAt: Date;
	date: Date;
}

export const KapSchema = new DBSchema({
	text: { type: String, required: true },
	focalPoint: { type: String, required: true },
	prayers: { type: [String], default: [] },
	date: {
		type: Date,
		required: true
	}
});

KapSchema.path("date").validate((date: Date) => {
	const kapRepo = new KapRepo();
	return new Promise((resolve, reject) => {
		kapRepo
			.dayExists(date)
			.then(exists => resolve(!exists))
			.catch(reject);
	});
}, "Kap for this day already exists");

const kap = model<IKap>("Kap", KapSchema);

export default kap;
