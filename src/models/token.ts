import moment from "moment";

const expiresIn = process.env.JWT_EXPIRES;

export default class JWTToken {
	public token: string;
	public expires: number;

	constructor(token: string, exp: number) {
		this.token = token;
		this.expires = exp;
	}
}
