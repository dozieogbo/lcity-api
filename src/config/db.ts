import mongoose from "mongoose";
import UserRepo from "../repo/user";
import { ADMIN } from "../util/constants";

const uri = process.env.DB_URI;

mongoose.Promise = global.Promise;

const init: () => void = async function() {
	if (uri) {
		try {
			await mongoose.connect(
				uri,
				{
					useNewUrlParser: true,
					useCreateIndex: true
				}
			);

			console.log("DB Connected");

			await seed();
		} catch (err) {
			console.log(err);
		}
	} else {
		throw new Error("DB URI not found. Please, add one to continue");
	}
};
const seed: () => void = async function() {
	const userRepo = new UserRepo();

	const hasAdmin = await userRepo.countByRole(ADMIN);

	if (!hasAdmin) {
		const data = {
			firstName: "LC",
			lastName: "Admin",
			email: "admin@lightworldcity.org",
			isAuthor: true,
			password: "12lcity.org345.",
			role: ADMIN
		};

		console.log("Seeding DB");

		await userRepo.add(data);
	}
};

export default {
	init
};
