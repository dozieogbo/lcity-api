import { Repo } from "./base";
import Kap, { IKap } from "../models/db/kap";

export default class KapRepo extends Repo<IKap> {
	constructor() {
		super(Kap);
	}

	getByDay: (date: Date) => Promise<IKap> = date => {
		const day = new Date(date.getFullYear(), date.getMonth(), date.getDate());
		const dayAfter = new Date(
			date.getFullYear(),
			date.getMonth(),
			date.getDate() + 1
		);

		return this.getSingleBy({
			date: { $lt: dayAfter, $gte: day }
		});
	};

	dayExists: (date: Date) => Promise<boolean> = async date => {
		const day = new Date(date.getFullYear(), date.getMonth(), date.getDate());
		const dayAfter = new Date(
			date.getFullYear(),
			date.getMonth(),
			date.getDate() + 1
		);

		const total = await this.count({
			date: { $lt: dayAfter, $gte: day }
		});

		return total > 0;
	};
}
