import { Document, Model, Query } from "mongoose";

export abstract class Repo<U extends Document> {
	private model: Model<U>;

	constructor(model: Model<U>) {
		this.model = model;
	}

	getAll: (page?: number, pageSize?: number) => Promise<U[]> = (
		page = 1,
		pageSize = 30
	) => {
		return this.model
			.find({})
			.skip((page - 1) * pageSize)
			.limit(pageSize)
			.exec();
	};
	getBy: (query: any, page?: number, pageSize?: number) => Promise<U[]> = (
		query,
		page = 1,
		pageSize = 30
	) => {
		return this.model
			.find(query)
			.skip((page - 1) * pageSize)
			.limit(pageSize)
			.exec();
	};
	getSingleBy: (
		query: any,
		page?: number,
		pageSize?: number
	) => Promise<U> = query => {
		return this.model.findOne(query).exec();
	};
	getById: (id: string) => Promise<U> = id => {
		return this.model.findById(id).exec();
	};
	add: (data: any) => Promise<U> = data => {
		return this.model.create(data);
	};
	update: (query: string, data: object) => Promise<U> = (query, data) => {
		return this.model.findByIdAndUpdate(query, data).exec();
	};
	updateMany: (query: object, data: U) => Promise<void> = async (
		query,
		data
	) => {
		await this.model.updateMany(query, data).exec();
	};
	delete: (query: string | object) => Promise<U> = query => {
		return this.model.findByIdAndDelete(query).exec();
	};
	deleteMany: (query: string) => Promise<void> = async query => {
		await this.model.deleteMany(query).exec();
	};
	count: (query?: any) => Promise<number> = (query = {}) => {
		return this.model.countDocuments(query).exec();
	};
}
