import { Repo } from "./base";
import User, { IUser } from "../models/db/user";

export default class UserRepo extends Repo<IUser> {
	constructor() {
		super(User);
	}

	getByEmail: (email: string) => Promise<IUser> = email => {
		return this.getSingleBy({ email });
	};

	getByRole: (
		role: string,
		page?: number,
		pageSize?: number
	) => Promise<IUser[]> = (role, page = 1, pageSize = 30) => {
		return this.getBy({ role }, page, pageSize);
	};

	countByRole: (role: string) => Promise<number> = role => {
		return this.count({ role });
	};

	getAuthors: (page?: number, pageSize?: number) => Promise<IUser[]> = (
		page = 1,
		pageSize = 30
	) => {
		return this.getBy({ isAuthor: true }, page, pageSize);
	};
}
