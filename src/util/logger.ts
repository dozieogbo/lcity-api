import { createLogger, transports, format, Logger } from "winston";

const logger: Logger = createLogger({
	level: "info",
	format: format.combine(
		format.timestamp({
			format: "YYYY-MM-DD HH:mm:ss"
		}),
		format.errors({ stack: true }),
		format.splat(),
		format.json()
	),
	transports: [
		new transports.Console({
			level: process.env.NODE_ENV === "production" ? "error" : "info"
		}),
		new transports.File({ filename: "logs/debug.log", level: "debug" }),
		new transports.File({ filename: "logs/error.log", level: "error" })
	]
});

if (process.env.NODE_ENV !== "production") {
	logger.debug("Logging initialized at debug level");
}

export default logger;
