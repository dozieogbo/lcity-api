export class NonUniqueEmailError extends Error {
	constructor() {
		super("The email has already been used.");
		this.name = "NonUniqueEmail";
	}
}

export class NonUniquePhoneError extends Error {
	constructor() {
		super("The phone no has already been used.");
		this.name = "NonUniquePhone";
	}
}

export class InvalidUserError extends Error {
	constructor() {
		super("The user does not exist.");
		this.name = "InvalidUser";
	}
}
