import { RequestHandler, Response, NextFunction } from "express";
import { validationResult } from "express-validator/check";
import { ADMIN } from "./constants";
import AuthService from "./authservice";
import IRequestHandler from "../models/requestHandler";

export const validate: RequestHandler = (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(400).json({
			errors: errors.array().map((err: any) => err.msg)
		});
	}
	return next();
};

export const hasRole: (...role: Array<string>) => IRequestHandler = (
	...role
) => {
	return (req, res, next) => {
		try {
			if (!req.user) throw new Error("User does not exist");

			if (!role.includes(req.user.role))
				throw new Error("Operation not allowed");

			next();
		} catch (err) {
			res.status(401).json({ message: err.message });
		}
	};
};

export const authenticate: IRequestHandler = async (req, res, next) => {
	try {
		const authHeader = req.headers.authorization;

		if (!authHeader) throw new Error("No authorization header");
		if (!authHeader.includes("Bearer")) throw new Error("No bearer auth");

		const token = authHeader.replace("Bearer ", "");

		const currentUser = await AuthService.verify(token);

		req.user = currentUser;
		req.isAdmin = currentUser.role === ADMIN;

		next();
	} catch (err) {
		res.status(401).json({ message: err.message });
	}
};

export const initialize: IRequestHandler = (req, res, next) => {
	res.ok = (data, message = "Ok") => {
		res.json({
			data,
			message
		});
	};

	res.invalid = (errors, message = "Bad Request") => {
		res.status(400).json({
			errors,
			message
		});
	};

	res.error = error => {
		res.status(500).json({
			message: typeof error === "string" ? error : error.message
		});
	};

	res.unauthorized = (message = "Unauthorized") => {
		res.status(401).json({ message });
	};

	next();
};
