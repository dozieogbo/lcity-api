import JWT from "./jwtservice";
import bcrypt from "bcrypt";
import { IUser } from "../models/db/user";
import UserRepo from "../repo/user";
import { InvalidUserError } from "./errors";

export default class AuthService {
	private static userRepo: UserRepo = new UserRepo();

	static verify: (token: string) => Promise<IUser> = async token => {
		if (!token) throw new Error("No token available");

		const id = await JWT.decode(token);

		const currentUser = await AuthService.userRepo.getById(id);

		if (!currentUser) throw new InvalidUserError();

		return currentUser;
	};

	static hashPassword: (
		password: string
	) => Promise<string> = async password => {
		return await bcrypt.hash(password, 5);
	};

	static verifyPassword: (
		password: string,
		hashedPassword: string
	) => Promise<boolean> = async (password, hashedPassword) => {
		return await bcrypt.compare(password, hashedPassword);
	};
}
