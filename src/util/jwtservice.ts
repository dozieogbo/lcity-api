import JWT from "jsonwebtoken";
import moment from "moment";
import JWTToken from "../models/token";
const jwtKey = process.env.JWT_KEY;
const expiresIn = `${process.env.JWT_EXPIRES}`;

export default class JWTService {
	static encode: (id: string) => Promise<JWTToken> = id => {
		return new Promise((resolve, reject) => {
			const exp = moment()
				.add(expiresIn, "days")
				.unix();

			JWT.sign({ id, exp }, jwtKey, (err: Error, token: string) => {
				if (err) reject(err);
				else resolve(new JWTToken(token, exp));
			});
		});
	};

	static decode: (id: string) => Promise<string> = token => {
		return new Promise((resolve, reject) => {
			JWT.verify(token, jwtKey, (err: JWT.VerifyErrors, decoded: any) => {
				if (err) {
					reject(err);
				} else {
					resolve(decoded.id);
				}
			});
		});
	};
}
