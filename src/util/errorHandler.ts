import { ErrorRequestHandler } from "express";
import { Error } from "mongoose";
import logger from "./logger";
import { IResponse } from "../models/requestHandler";
import { NonUniqueEmailError } from "./errors";

const isProduction = process.env.NODE_ENV === "production";

const handler: ErrorRequestHandler = (err, req, res: IResponse, next) => {
	if (err instanceof Error.ValidationError) {
		const errors = Object.values(err.errors).map(e => e.message);
		res.invalid(errors, "Validation error occured");
	} else if (err instanceof NonUniqueEmailError) {
		res.invalid([err.message], err.message);
	} else {
		const message = isProduction
			? "An unexpected error has occured. Please, contact the administrator"
			: err.message;

		res.status(500).json({
			message
		});
	}

	logger.error(err.message);

	if (!isProduction) console.error(err);
};

export default handler;
