import { Router, RequestHandler } from "express";

export default (router: Router) => {
	const getUsers: RequestHandler = (req, res, next) => {
		res.send("respond with a resource");
	};

	/* GET users listing. */
	router.get("/", getUsers);
};
