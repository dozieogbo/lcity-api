import { Router, Request, Response } from "express";
const router = Router();

import kapRoutes from "./kap";
import feedRoutes from "./feed";
import authRoutes from "./auth";

router.use("/api/kaps", kapRoutes);
router.use("/api/feed", feedRoutes);
router.use("/api/auth", authRoutes);

/* GET home page. */
router.get("/", function(req: Request, res: Response) {
	res.render("index", { title: "Express" });
});

export default router;
