import { Router } from "express";
import { check } from "express-validator/check";
import AuthController from "../controllers/auth";
import { validate } from "../util/middleware";

const router: Router = Router();
const authController = new AuthController();

router
	.post(
		"/login",
		[check("username").exists(), check("password").exists()],
		authController.login
	)
	.post(
		"/signup",
		[
			check("firstName").exists(),
			check("lastName").exists(),
			check("email")
				.exists()
				.isEmail(),
			check("password").exists()
		],
		validate,
		authController.signup
	)
	.post(
		"/passwords/change",
		[check("password").exists(), check("newPassword").exists()],
		validate,
		authController.changePwd
	);

export default router;
