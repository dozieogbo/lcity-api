import { Router } from "express";
import UtilController from "../controllers/util";

const router = Router();
const utilController = new UtilController();

router.get("", utilController.getFeed);

export default router;
