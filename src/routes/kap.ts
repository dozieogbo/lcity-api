import { Router } from "express";
import { check } from "express-validator/check";
import KapController from "../controllers/kap";
import { validate } from "../util/middleware";

const router: Router = Router();
const kapController = new KapController();

router
	.route("")
	.get(kapController.getAllKaps)
	.post(
		[
			check("focalPoint").exists(),
			check("text").exists(),
			check("date").exists(),
			check("prayers")
				.isArray()
				.not()
				.isEmpty()
		],
		validate,
		kapController.createKap
	);

export default router;
